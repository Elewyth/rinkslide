package ch.elewyth.mobile;

public class Point {

	private final int column;
	private final int row;
	
	public Point(int column, int row) {
		this.column = column;
		this.row = row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public int getRow() {
		return row;
	}
	
	public static Point fromUI(int row, int column, int numRows) {
		return new Point(column + 1, numRows - row);
	}
	
	public static Point offset(Point origin, Point offset) {
		return new Point(origin.column + offset.column, origin.row + offset.row);
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == this) { return true; }
		if (!(other instanceof Point)) { return false; }
		return column == ((Point) other).column && row == ((Point) other).row; 
	}
	
	@Override
	public int hashCode() {
		return 23 + column * 37 + row * 19;
	}
	
	@Override
	public String toString() {
		return "[" + column + "," + row + "]";
	}
}
