package ch.elewyth.mobile;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Holds the current state of the game, i.e. Rink positions, Player positions and Player turn.
 */
public class GameState {

	private final boolean[][] rinks;
	private Point player1, player2;
	private boolean player1Turn = true;
	private final Set<Point> goals = new HashSet<>();
	private boolean goalReached = false;
	
	public GameState(boolean[][] rinks, Set<Point> goals, Point player1, Point player2) {
		this.rinks = new boolean[rinks.length][rinks[0].length];
		for (int i = 0; i < rinks.length; i++) {
			this.rinks[i] = Arrays.copyOfRange(rinks[i], 0, rinks[i].length);
		}
		
		this.goals.addAll(goals);
		
		if (!hasRink(player1)) {
			throw new IllegalArgumentException("Player 1 position is not on a Rink");
		}
		
		if (!hasRink(player2)) {
			throw new IllegalArgumentException("Player 2 position is not on a Rink");
		}
		
		this.player1 = player1;
		this.player2 = player2;
	}
	
	public boolean hasRink(Point point) {
		return rinks[rinks.length - point.getRow()][point.getColumn() - 1];
	}
	
	public boolean hasPlayer(Point point) {
		return player1.equals(point) || player2.equals(point);
	}
	
	public boolean isInPlayingField(Point point) {
		return point.getColumn() >= 1 && point.getRow() >= 1
				&& point.getColumn() <= rinks[0].length && point.getRow() <= rinks.length;
	}
	
	public Point getPlayer1() {
		return player1;
	}
	
	public void movePlayer1(Point newLocation) {
		this.player1 = newLocation;
	}
	
	public Point getPlayer2() {
		return player2;
	}
	
	public void movePlayer2(Point newLocation) {
		this.player2 = newLocation;
	}
	
	public void moveActivePlayer(Point newLocation) {
		if (player1Turn) {
			movePlayer1(newLocation);
		} else {
			movePlayer2(newLocation);
		}
		
		if (goals.contains(newLocation)) {
			goalReached = true;
		}
	}
	
	public boolean isPlayer1Turn() {
		return player1Turn;
	}
	
	public Point getActivePlayer() {
		return isPlayer1Turn() ? player1 : player2;
	}

	public void moveRink(Point origin, Point activePlayer) {
		rinks[rinks.length - origin.getRow()][origin.getColumn() - 1] = false;
		rinks[rinks.length - activePlayer.getRow()][activePlayer.getColumn() - 1] = true;
	}

	public void endTurn() {
		player1Turn = !player1Turn;		
	}

	public boolean isGoal(Point field) {
		return goals.contains(field);
	}

	public boolean isGoalReached() {
		return goalReached;
	}
}
